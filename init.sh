#!/bin/sh

cd $HOME

# install apt packages
sudo apt update
sudo apt install -y zsh zsh wget nvim

# install oh-my-posh (shell theme)
sudo wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh
sudo chmod +x /usr/local/bin/oh-my-posh

# get oh-my-posh theme
wget https://gist.githubusercontent.com/sudame/58ec2b38428fb244ddd4ca21e190a100/raw/85ede19ae484d6b72d37b9879a7bcfad10dbac15/theme.json
mkdir .conf
mv theme.json .conf/omp.theme.json

# get .zshrc
wget https://gist.githubusercontent.com/sudame/1e0894c720af8b166cd6bb4752465e43/raw/4ebe45cce4d1cc690688f5d51a5dbb93dfac163d/.zshrc

# install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# install better ls
$HOME/.cargo/bin/cargo install lsd

# write theme to .zshrc
echo 'eval "$(oh-my-posh --init --shell zsh --config ~/.conf/omp.theme.json)"' >> ~/.zshrc